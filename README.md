A game created for a 10-day-long GameJam made during <AcademiaDeCódigo_> 14-week bootcamp.

The game consists of finding the way to the flag with some obstacles in the way.

Instructions:
 - Arrows Keys for movement
 - H, J, K and L to change pokemon:
    H - Bulbasaur
    J - Squirtle
    K - Charmander
    L - Pikachu

Technologies used:
 - Java
 - Simple Graphics Library
